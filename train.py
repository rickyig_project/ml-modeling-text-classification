import pickle
import yaml
import os
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import classification_report
import neptune
from dotenv import load_dotenv
import joblib

load_dotenv()

run = neptune.init_run(project=os.getenv('NEPTUNE_PROJECT_NAME'))

data_path = 'data/text-classification-data-management-system/data/'

def load_pickle(child_path):
    data = pickle.load(open(os.path.join(data_path, child_path), 'rb'))
    return data

# Function to convert string numbers to appropriate numeric types
def convert_params(config):
    for key, value in config.items():
        try:
            # Attempt to convert string numbers to floats
            config[key] = int(value)
        except ValueError:
            # Keep the original value if conversion is not possible
            pass
        except TypeError:
            pass
    return config

train_data = load_pickle("train_data.pickle")
test_data = load_pickle("test_data.pickle")

train_x = train_data['x']
train_y = train_data['y']

test_x = test_data['x']
test_y = test_data['y']


train_x = train_x.copy()
train_y = train_y.copy()

train_y = np.array(train_y)
test_y = np.array(test_y)

# Load methods config from YAML
with open("config.yaml", "r") as file:
    config = yaml.safe_load(file)

# Dictionary to map method names to sklearn models
model_mapping = {
    "Random Forest": RandomForestClassifier,
    "SVM": SVC,
    "Logistic Regression": LogisticRegression
}
choosen_method = config['choosen_method']
model_version = config['model_version']

print(f"Training with {choosen_method}")

method = [x for x in config['methods'] if x['name'] == choosen_method][0]


# # Instantiate and train the Logistic Regression model
# model = LogisticRegression(max_iter=200)  # Increase max_iter if the default converges too slowly
# model.fit(train_x, train_y)

# # Predict the labels for the test set
# predicted_y = model.predict(test_x)

# # Print the classification report
# class_labels = {'politics':0, 'sport':1, 'tech':2, 'entertainment':3, 'business':4}
# report = classification_report(test_y, predicted_y, target_names=list(class_labels.keys()))
# print(report)

model_name = method["name"]
model_config = convert_params(method["config"])

model_namespace = f"models/{choosen_method}/{model_version}"

for param_name, param_value in model_config.items():
    run[f"{model_namespace}/parameters/{param_name}"] = param_value

# Instantiate model with specified config
ModelClass = model_mapping[model_name]
model = ModelClass(**model_config)

# Train model
model.fit(train_x, train_y)

# Save the model to a file
model_filename = f"{model_name}_{model_version}.joblib"
joblib.dump(model, model_filename)

# Upload model to Neptune
run[f"{model_namespace}/{model_filename}"].upload(model_filename)

# Predict and evaluate
predicted_y = model.predict(test_x)
class_labels = {'politics':0, 'sport':1, 'tech':2, 'entertainment':3, 'business':4}
report = classification_report(test_y, predicted_y, target_names=list(class_labels.keys()))

# Log classification report to Neptune
run[f"{model_namespace}/classification_report"] = report

print(f"Classification Report for {model_name}:\n{report}\n")

# Vectorizer to neptune.ai
# Save the Vectorizer to a file
loaded_vectorizer = load_pickle("vectorizer.pickle")
choosen_vectorizer_method = config['choosen_vectorizer_method']
vectorizer_version = config['vectorizer_version']

vectorizer_filename = f"{choosen_vectorizer_method}_{vectorizer_version}.joblib"
joblib.dump(loaded_vectorizer, vectorizer_filename)

vectorizer_namespace = f"vectorizers/{choosen_vectorizer_method}/{vectorizer_version}"
run[f"{vectorizer_namespace}/{vectorizer_filename}"].upload(vectorizer_filename)

# vectorizer_mapping = {
#     "TF-IDF": TfidfVectorizer,
# }
vectorizer_method = [x for x in config['vectorizer_methods'] if x['vectorizer_name'] == choosen_vectorizer_method][0]

vectorizer_name = vectorizer_method["vectorizer_name"]
vectorizer_config = convert_params(vectorizer_method["vectorizer_config"])

for vectorizer_param_name, vectorizer_param_value in vectorizer_config.items():
    run[f"{vectorizer_namespace}/parameters/{vectorizer_param_name}"] = vectorizer_param_value

run.stop()